package com.neelam.surveysdk.listener;

/**
 * Created by Neelam on 8/27/2016.
 */
public interface CbChangedListener {
    /**
     * Get called when checkbox or radio button state is changed.
     * @param questionId
     * @param optionID
     * @param isChecked
     *  @param isSingleChoice
     */
    public void onCbChanged(String questionId,String optionID,boolean isChecked,boolean isSingleChoice);
}
