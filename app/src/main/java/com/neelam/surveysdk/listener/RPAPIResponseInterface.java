package com.neelam.surveysdk.listener;

import org.json.JSONObject;

/**Created by Neelam**/
public interface RPAPIResponseInterface {
    /**
     * Give the response of called web service(API).
     * @param response JSONObject object in response.
     * @param type  type is representing a int number in behalf of API calling(Which API you called and get response).
     */
    void onGetAPIResponse(JSONObject response, int type);

    /**
     * Give the error response of web service(API).
     * @param error  error response.
     * @param code
     * @param type
     */
    void onGetAPIErrorResponse(String error, int code, int type);

}
