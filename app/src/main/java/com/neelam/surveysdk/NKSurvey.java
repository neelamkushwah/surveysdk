package com.neelam.surveysdk;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;

import com.neelam.surveysdk.listener.RPAPIResponseInterface;
import com.neelam.surveysdk.utility.NKConstants;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.FormBody;
import okhttp3.RequestBody;

/**
 * Created by Neelam on 8/28/2016.
 */
public  class NKSurvey implements RPAPIResponseInterface {

    /*This launch the survey screen if notification is coming from survey
    **@param ctx
     * @param bundle**/
    private static String token;
    private static Context ctx;

    public static void launchSurveySDK(Context ctx, Bundle bundle) {
        if (bundle != null) {
            if (bundle.containsKey("isFrom")) {
                if (bundle.getString("isFrom").equals("Survey")) {
                    Intent i = new Intent(ctx, NKSurveyActivity.class);
                    i.putExtra("data", bundle.getString("data"));
                    ctx.startActivity(i);

                }

            }
        }
    }

    public  void registerWithSurvey(Context ctx,String fcmToken) {
        token = fcmToken;
        NKSurvey.ctx=ctx;
        //api
        String url = NKConstants.URL.BASE_URL + NKConstants.URL.REGISTEDEVICE_URL;
        String unique_id = Settings.Secure.getString(ctx.getContentResolver(), Settings.Secure.ANDROID_ID);
        RequestBody requestBody = new FormBody.Builder().
                add("deviceId", unique_id).add("fcmToken", token)
                .build();
        RPApiCaller.callAPIPostRequest(ctx, false, requestBody,NKSurvey.this, url,
                NKConstants.DO_REGISTER);
    }

    /**
     * Get 200 ok  response from server.
     *
     * @param response JSONObject body
     * @param type
     */
    @Override
    public void onGetAPIResponse(JSONObject response, int type) {
        try {
            Log.i("SURVEYSUCCESS", response.getString("message"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    /**
     * Get error  response from server.It can be anything connection time or socket time out error
     *
     * @param error JSONObject body
     * @param type
     * @param type
     */
    @Override
    public void onGetAPIErrorResponse(String error, int code, int type) {
        Log.i("SURVEYERROR", error);
        registerWithSurvey(ctx,token);
    }
}
