package com.neelam.surveysdk.model;

import java.util.ArrayList;

/**
 * Created by Neelam on 8/27/2016.
 */
public class AnswerModel {
    private String questionId;
    private ArrayList<String> answers;

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public ArrayList<String> getAnswers() {
        return answers;
    }

    public void setAnswers(ArrayList<String> answers) {
        this.answers = answers;
    }
}
