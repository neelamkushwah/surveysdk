package com.neelam.surveysdk.model;

/**
 * Created by Neelam on 8/24/2016.
 */
public class OptionModel {
    private String optionId;
    private String optionValue;

    public String getOptionId() {
        return optionId;
    }

    public void setOptionId(String optionId) {
        this.optionId = optionId;
    }

    public String getOptionValue() {
        return optionValue;
    }

    public void setOptionValue(String optionValue) {
        this.optionValue = optionValue;
    }
}
