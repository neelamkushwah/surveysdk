package com.neelam.surveysdk.model;

import java.util.ArrayList;

/**
 * Created by Neelam on 8/26/2016.
 */
public class QuestionModel {
    private String questionValue;
    private String questionType;
    private String questionID;
    private ArrayList<OptionModel> options;

    public String getQuestionValue() {
        return questionValue;
    }

    public void setQuestionValue(String questionValue) {
        this.questionValue = questionValue;
    }

    public String getQuestionType() {
        return questionType;
    }

    public void setQuestionType(String questionType) {
        this.questionType = questionType;
    }

    public String getQuestionID() {
        return questionID;
    }

    public void setQuestionID(String questionID) {
        this.questionID = questionID;
    }

    public ArrayList<OptionModel> getOptions() {
        return options;
    }

    public void setOptions(ArrayList<OptionModel> options) {
        this.options = options;
    }
}