package com.neelam.surveysdk.model;

import java.util.ArrayList;

/**
 * Created by Neelam on 8/24/2016.
 */
public class SurveyQuestionModel {

    private ArrayList<QuestionModel> data;

    public ArrayList<QuestionModel> getData() {
        return data;
    }

    public void setData(ArrayList<QuestionModel> data) {
        this.data = data;
    }


}
