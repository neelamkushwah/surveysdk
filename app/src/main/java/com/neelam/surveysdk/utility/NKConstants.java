package com.neelam.surveysdk.utility;

/**
 * Created by Neelam on 8/25/2016.
 */
public interface NKConstants {
    public static int API_ERROR_RESPONSE_NULL = 900;
    public static int API_ERROR_TIME_OUT = 901;
    public static int API_ERROR_IO_EXCEPTION = 902;
    public static int API_ERROR_EXCEPTION = 903;
    public static int DO_REGISTER = 1;

    interface URL {
        public String BASE_URL = "http://52.10.48.108/Survey/survey/";
        public String REGISTEDEVICE_URL = "RegisterDevice.php";
        public String SUBMITANSWERS_URL = "answer.php";

    }
}
