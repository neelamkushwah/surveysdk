package com.neelam.surveysdk;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.neelam.surveysdk.listener.RPAPIResponseInterface;
import com.neelam.surveysdk.utility.NKConstants;

import org.json.JSONObject;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**Created by Neelam on 08/27/2016**/
public class RPApiCaller {
    private static OkHttpClient client = new OkHttpClient.Builder()
            .connectTimeout(50, TimeUnit.SECONDS)
            .build();
    private static Context ctx;
    private static ProgressDialog dialog;

    public static void callAPIGetRequest(final Context context,final  boolean showDialog, final RPAPIResponseInterface anInterface,
                                         final String url, final int urlNo) {

        ctx = context;
        new AsyncTask<String, JSONObject, JSONObject>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                if (showDialog) {
                    dialog = new ProgressDialog(context);
                    dialog.setCancelable(false);
                    dialog.setMessage("Please wait..");
                    dialog.show();
                }
            }

            @Override
            protected JSONObject doInBackground(String... params) {
                JSONObject jsonObject = null;
                try {
                    String response = GETResponse(client, url, anInterface, urlNo);
                    if (response != null)
                        jsonObject = new JSONObject(response);

                } catch (Exception e) {
                    jsonObject = null;
                    e.printStackTrace();
                }
                return jsonObject;
            }

            @Override
            protected void onPostExecute(JSONObject jsonObject) {
                super.onPostExecute(jsonObject);
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (jsonObject != null) {
                    anInterface.onGetAPIResponse(jsonObject, urlNo);
                } else {
                    anInterface.onGetAPIErrorResponse("ERROR", 900, urlNo);

                }


            }
        }.execute();
    }

    public static void callAPIPostRequest(final Context context, final boolean showDialog, final RequestBody requestBody,
                                          final RPAPIResponseInterface anInterface, final String url,
                                          final int urlNo) {
        ctx = context;
        new AsyncTask<String, JSONObject, JSONObject>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                if (showDialog) {
                    dialog = new ProgressDialog(context);
                    dialog.setCancelable(false);
                    dialog.setMessage("Please wait..");
                    dialog.show();
                }

            }

            @Override
            protected JSONObject doInBackground(String... params) {
                JSONObject jsonObject = null;
                try {
                    String response = POSTResponse(client, url, requestBody, anInterface, urlNo);
                    if (response != null)
                        jsonObject = new JSONObject(response);
                } catch (Exception e) {
                    jsonObject = null;
                    e.printStackTrace();
                }
                return jsonObject;
            }

            @Override
            protected void onPostExecute(JSONObject jsonObject) {
                super.onPostExecute(jsonObject);
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (jsonObject != null) {
                    anInterface.onGetAPIResponse(jsonObject, urlNo);
                } else {
                    anInterface.onGetAPIErrorResponse("ERROR", 900, urlNo);

                }
            }
        }.execute();
    }

    //GET network request
    public static String GETResponse(OkHttpClient client, String url, RPAPIResponseInterface anInterface, int urlNo) {
        Response response = null;
        try {
            Request request = new Request.Builder()
                    .url(url).addHeader("Content-type","text/html")
                    .build();
            response = client.newCall(request).execute();
            if (response.isSuccessful()) {
                return response.body().string();
            } else {
                anInterface.onGetAPIErrorResponse("ERROR", NKConstants.API_ERROR_RESPONSE_NULL, urlNo);
                return null;

            }
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            anInterface.onGetAPIErrorResponse("SocketTimeout", NKConstants.API_ERROR_TIME_OUT, urlNo);
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            anInterface.onGetAPIErrorResponse(e.getMessage(), NKConstants.API_ERROR_IO_EXCEPTION, urlNo);
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            anInterface.onGetAPIErrorResponse(e.getMessage(), NKConstants.API_ERROR_EXCEPTION, urlNo);
            return null;
        }
    }


    public static String POSTResponse(OkHttpClient client, String url, RequestBody body,
                                      RPAPIResponseInterface anInterface, int urlNo) {
        Response response = null;
        try {
            Request request = null;


            request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .build();


            response = client.newCall(request).execute();
            if (response.isSuccessful()) {
                return response.body().string();
            } else {
                anInterface.onGetAPIErrorResponse("ERROR", NKConstants.API_ERROR_RESPONSE_NULL, urlNo);
                return null;
            }
        } catch (SocketTimeoutException e) {
            e.printStackTrace();

            anInterface.onGetAPIErrorResponse("SocketTimeout", NKConstants.API_ERROR_TIME_OUT, urlNo);
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            anInterface.onGetAPIErrorResponse(e.getMessage(), NKConstants.API_ERROR_IO_EXCEPTION, urlNo);
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            anInterface.onGetAPIErrorResponse(e.getMessage(), NKConstants.API_ERROR_EXCEPTION, urlNo);
            return null;
        }
    }
}