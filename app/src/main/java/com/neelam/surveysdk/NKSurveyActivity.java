

package com.neelam.surveysdk;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.neelam.survey.R;
import com.neelam.surveysdk.adapter.QuestionAnswersAdapter;
import com.neelam.surveysdk.listener.CbChangedListener;
import com.neelam.surveysdk.listener.RPAPIResponseInterface;
import com.neelam.surveysdk.model.QuestionModel;
import com.neelam.surveysdk.model.SurveyQuestionModel;
import com.neelam.surveysdk.utility.NKConstants;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.FormBody;
import okhttp3.RequestBody;
/**
 * Created by Neelam on 8/28/2016.
 */
public class NKSurveyActivity extends AppCompatActivity implements View.OnClickListener, CbChangedListener, RPAPIResponseInterface {

    private JSONObject jsonResp = new JSONObject();

    private static final String TAG = "NKSurveyActivity";
    private TextView tvQuestions;
    private RecyclerView recyclerView;
    private Button btnPrevious;
    private Button btnNext;
    private Context ctx;
    private LinearLayout llButton;
    private SurveyQuestionModel surveyQuestionModel;
    private QuestionAnswersAdapter adapter;
    private QuestionModel questionModel;
    private int pos = 0;
    private ImageView ivClose;


  /**@param savedInstanceState**/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_survey);
        ctx = this;
        initViews();
        getGSON();

    }
/**Get gson from data received from intent***/
    private void getGSON() {


        Log.i("J", getIntent().getStringExtra("data"));

        Gson gson = new Gson();
        surveyQuestionModel = gson.fromJson(getIntent().getStringExtra("data"), SurveyQuestionModel.class);

        setData();


    }
/**Set data for the adapter, all logics for next and previous are written on this method**/
    private void setData() {

        tvQuestions.setText("Q) " + surveyQuestionModel.getData().get(pos).getQuestionValue());

        questionModel = new QuestionModel();

        questionModel = (surveyQuestionModel.getData().get(pos));
        ArrayList<Integer> arrChecked = new ArrayList<>();
        if (jsonResp.has(questionModel.getQuestionID())) {

            for (int i = 0; i < questionModel.getOptions().size(); i++) {
                try {
                    String[] strarray = jsonResp.getString(questionModel.getQuestionID()).split(",");
                    for (int j = 0; j < strarray.length; j++) {
                        if (strarray[j].equals(questionModel.getOptions().get(i).getOptionId()))
                            arrChecked.add(i);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        // if (adapter == null) {
        adapter = new QuestionAnswersAdapter(ctx, questionModel, arrChecked);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
//        } else {
//            adapter.notifyDataSetChanged();
//
//        }
        if (surveyQuestionModel.getData().size() == 1) {
            btnNext.setText("SUBMIT");
            llButton.setWeightSum(1);
            btnPrevious.setVisibility(View.GONE);
        } else if (pos + 1 == surveyQuestionModel.getData().size()) {
            btnNext.setText("SUBMIT");
            llButton.setWeightSum(2);
            btnPrevious.setVisibility(View.VISIBLE);
        } else if (pos == 0) {
            btnNext.setText("NEXT");
            llButton.setWeightSum(1);
            btnPrevious.setVisibility(View.GONE);
        } else {
            btnNext.setText("NEXT");
            llButton.setWeightSum(2);
            btnPrevious.setVisibility(View.VISIBLE);
        }
        pos++;
    }

    private void initViews() {
        tvQuestions = (TextView) findViewById(R.id.tvQuestions);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        btnPrevious = (Button) findViewById(R.id.btnPrevious);
        btnNext = (Button) findViewById(R.id.btnNext);
        llButton = (LinearLayout) findViewById(R.id.llButton);
        ivClose = (ImageView) findViewById(R.id.ivClose);
        btnNext.setOnClickListener(this);
        btnPrevious.setOnClickListener(this);
        ivClose.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btnNext) {
            if (jsonResp.has(surveyQuestionModel.getData().get(pos - 1).getQuestionID())) {
                if (pos < surveyQuestionModel.getData().size()) {
                    setData();
                } else {
                    callSubmitAnswerAPI();
                }
            } else {
                Toast.makeText(ctx, "Please choose atleast one option", Toast.LENGTH_LONG).show();
            }
        } else if (v.getId() == R.id.btnPrevious) {
            pos = pos - 2;
            setData();
        } else {
            finish();
        }
    }
/***APi for submitting naswer to the server**/
    private void callSubmitAnswerAPI() {
        String unique_id = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);

        String url = NKConstants.URL.BASE_URL + NKConstants.URL.SUBMITANSWERS_URL;
        RequestBody requestBody = new FormBody.Builder().
                add("deviceID", unique_id).add("data", jsonResp.toString())
                .build();
        RPApiCaller.callAPIPostRequest(NKSurveyActivity.this, true, requestBody, NKSurveyActivity.this, url, 1);
    }

    /**This is the call abck method used for managing answers with respect to its question id **/
    @Override
    public void onCbChanged(String questionId, String optionID, boolean isChecked, boolean isSingleChoice) {
        try {
            if (isSingleChoice) {
                if (jsonResp.has(questionId)) {
                    jsonResp.remove(questionId);
                }
                if (isChecked) {


                    jsonResp.put(questionId, optionID);
                }

            } else {
                if (isChecked) {
                    if (jsonResp.has(questionId)) {
                        optionID = jsonResp.getString(questionId) + "," + optionID;
                        jsonResp.remove(questionId);
                        jsonResp.put(questionId, optionID);
                    } else {
                        jsonResp.put(questionId, optionID);
                    }
                } else if (jsonResp.getString(questionId).equals(optionID)) {
                    jsonResp.remove(questionId);
                } else {
                    if (jsonResp.getString(questionId).contains("," + optionID)) {
                        optionID = jsonResp.getString(questionId).replace("," + optionID, "");
                    } else {
                        optionID = jsonResp.getString(questionId).replace(optionID + ",", "");
                    }
                    jsonResp.put(questionId, optionID);
                }

            }
            Log.i("JSON", jsonResp.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    /**
     * Get 200 ok  response from server.
     *
     * @param response JSONObject body
     * @param type

     */
    @Override
    public void onGetAPIResponse(JSONObject response, int type) {
        try {
            if (response.getInt("status") == 1) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
                builder.setMessage(response.getString("message"));
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        NKSurveyActivity.this.finish();
                    }
                });
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Get error  response from server.It can be anything connection time or socket time out error
     *
     * @param error JSONObject body
     * @param type
     * @param  type

     */
    @Override
    public void onGetAPIErrorResponse(String error, int code, int type) {
        Toast.makeText(ctx, "Somthing went wrong.Please try again later", Toast.LENGTH_LONG).show();
    }

}