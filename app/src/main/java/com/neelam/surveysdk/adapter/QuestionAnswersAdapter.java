package com.neelam.surveysdk.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;

import com.neelam.survey.R;
import com.neelam.surveysdk.listener.CbChangedListener;
import com.neelam.surveysdk.model.OptionModel;
import com.neelam.surveysdk.model.QuestionModel;

import java.util.ArrayList;

/**
 * Created by Neelam on 8/24/2016.
 */
public class QuestionAnswersAdapter extends RecyclerView.Adapter<QuestionAnswersAdapter.MyViewHolder> {
    private QuestionModel questionModel;
    private Context ctx;
    private  RadioButton lastCheckedRB;
    private CbChangedListener listener;
    private boolean isSingleChoice;
    private int pos;
    private ArrayList<Integer> arrChecked;

    /**Constructor
     * @param  ctx
     * @param questionModel
     * @param arrChecked**/
    public QuestionAnswersAdapter(Context ctx, QuestionModel questionModel, ArrayList<Integer> arrChecked) {
        this.questionModel = questionModel;
        listener = (CbChangedListener) ctx;
        this.ctx = ctx;
        this.arrChecked=arrChecked;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_options_listitems, parent, false);
        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        OptionModel model = questionModel.getOptions().get(position);
        isSingleChoice = false;
        pos = position;

        if (questionModel.getQuestionType().equals("1"))
            isSingleChoice = true;
        if (isSingleChoice) {
            if(arrChecked.size()>0){
            if(position==arrChecked.get(0)){
                holder.rbSingleChoice.setChecked(true);
                lastCheckedRB=holder.rbSingleChoice;
            }}
            holder.rbSingleChoice.setText(model.getOptionValue());
            holder.rbSingleChoice.setTag(position);
            holder.cbMultipleChoice.setVisibility(View.GONE);
            holder.rbSingleChoice.setVisibility(View.VISIBLE);
        } else {
            for(int i=0;i<arrChecked.size();i++){
                if(position==arrChecked.get(i)){
                    holder.cbMultipleChoice.setChecked(true);
                }
            }
            holder.cbMultipleChoice.setText(model.getOptionValue());
            holder.cbMultipleChoice.setTag(position);
            holder.rbSingleChoice.setVisibility(View.GONE);
            holder.cbMultipleChoice.setVisibility(View.VISIBLE);
        }

        holder.cbMultipleChoice.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                listener.onCbChanged(questionModel.getQuestionID(), questionModel.getOptions().get(position).getOptionId(),
                        b, isSingleChoice);
            }
        });

    }

    View.OnClickListener rbClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            RadioButton checked_rb = (RadioButton) v;
            if (checked_rb.isChecked()) {
                if (lastCheckedRB != null) {
                    lastCheckedRB.setChecked(false);
                }
                lastCheckedRB = checked_rb;

            }
            listener.onCbChanged(questionModel.getQuestionID(), questionModel.getOptions().get(Integer.parseInt(checked_rb.getTag().toString())).getOptionId(),
                    checked_rb.isChecked(), isSingleChoice);
        }
    };

    @Override
    public int getItemCount() {
        return questionModel.getOptions().size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public CheckBox cbMultipleChoice;
        public RadioButton rbSingleChoice;

        public MyViewHolder(View view) {
            super(view);
            rbSingleChoice = (RadioButton) view.findViewById(R.id.rbOptions);
            cbMultipleChoice = (CheckBox) view.findViewById(R.id.cbOptions);
            rbSingleChoice.setOnClickListener(rbClick);
        }
    }
}
